#include <QtGui>
#include <QErrorMessage>
#include <QFont>
#include <QtGui/QApplication>

#include "workReport.h"

Dialog::Dialog()
{

	viseInfoBox = new QGroupBox(tr("Исполнитель-заказчик"));
	QFormLayout *layoutViseInfo = new QFormLayout;
	// общие поля
	customer = new QLineEdit();
	employee = new QLineEdit();
	// добавляем поля на слой
	layoutViseInfo->addRow(new QLabel(tr("Заказчик:")), customer);
	layoutViseInfo->addRow(new QLabel(tr("Исполнитель:")), employee);
	viseInfoBox->setLayout(layoutViseInfo);

	commonInfoBox = new QGroupBox(tr("Общая информация"));
	QFormLayout *layoutCommonInfo = new QFormLayout;
	// общие поля
	//customer = new QLineEdit();
	defect = new QLineEdit();
	// добавляем поля на слой
	//layoutCommonInfo->addRow(new QLabel(tr("Заказчик:")), customer);
	layoutCommonInfo->addRow(new QLabel(tr("Описание дефекта:")), defect);
	commonInfoBox->setLayout(layoutCommonInfo);

	carInfoBox = new QGroupBox(tr("Авто"));
	QFormLayout *layoutCarInfo = new QFormLayout;
	car_model = new QLineEdit();
	car_age = new QLineEdit();
	car_milage = new QLineEdit();
	car_number = new QLineEdit();
	car_engine = new QLineEdit();
	car_chassis = new QLineEdit();
	car_vin = new QLineEdit();
	// добавляем поля на слой
	layoutCarInfo->addRow(new QLabel(tr("Марка, модель:")), car_model);
	layoutCarInfo->addRow(new QLabel(tr("Год выпуска:")), car_age);
	layoutCarInfo->addRow(new QLabel(tr("Пробег:")), car_milage);
	layoutCarInfo->addRow(new QLabel(tr("Государственный рег.номер:")), car_number);
	layoutCarInfo->addRow(new QLabel(tr("Двигатель №:")), car_engine);
	layoutCarInfo->addRow(new QLabel(tr("Шасси №:")), car_chassis);
	layoutCarInfo->addRow(new QLabel(tr("VIN:")), car_vin);
	carInfoBox->setLayout(layoutCarInfo);

	workInfoBox = new QGroupBox(tr("Работы"));
	QGridLayout *layoutWorkInfo = new QGridLayout;
	for( int i=0; i < WORK_NUM; i++ ) {
		work_code[i] = new QLineEdit();
		work_about[i] = new QLineEdit();
		work_num[i] = new QLineEdit();
		work_time[i] = new QLineEdit();
		work_cost[i] = new QLineEdit();
		layoutWorkInfo->addWidget(work_code[i],i+2,1,Qt::AlignLeft);
		layoutWorkInfo->addWidget(work_about[i],i+2,2,Qt::AlignLeft);
		layoutWorkInfo->addWidget(work_time[i],i+2,3,Qt::AlignLeft);
		layoutWorkInfo->addWidget(work_num[i],i+2,4,Qt::AlignLeft);
		layoutWorkInfo->addWidget(work_cost[i],i+2,5,Qt::AlignLeft);
	}
	// добавляем поля на слой
	layoutWorkInfo->addWidget(new QLabel(tr("Код")),1,1,Qt::AlignLeft);
	layoutWorkInfo->addWidget(new QLabel(tr("Наименование работ")),1,2,Qt::AlignLeft);
	layoutWorkInfo->addWidget(new QLabel(tr("Норма времени н/ч")),1,3,Qt::AlignLeft);
	layoutWorkInfo->addWidget(new QLabel(tr("Кол-во")),1,4,Qt::AlignLeft);
	layoutWorkInfo->addWidget(new QLabel(tr("Стоимость(руб.)")),1,5,Qt::AlignLeft);
	//layoutWorkInfo->addRow(new QLabel(tr("Марка, модель:")), work_code_1);
	workInfoBox->setLayout(layoutWorkInfo);

	sparesInfoBox = new QGroupBox(tr("Запасные части"));
	QGridLayout *layoutSparesInfo = new QGridLayout;
	for( int i=0; i < WORK_NUM; i++ ) {
		spares_code[i] = new QLineEdit();
		spares_about[i] = new QLineEdit();
		spares_units[i] = new QLineEdit();
		spares_num[i] = new QLineEdit();
		spares_cost[i] = new QLineEdit();
		layoutSparesInfo->addWidget(spares_code[i],i+2,1,Qt::AlignLeft);
		layoutSparesInfo->addWidget(spares_about[i],i+2,2,Qt::AlignLeft);
		layoutSparesInfo->addWidget(spares_units[i],i+2,3,Qt::AlignLeft);
		layoutSparesInfo->addWidget(spares_num[i],i+2,4,Qt::AlignLeft);
		layoutSparesInfo->addWidget(spares_cost[i],i+2,5,Qt::AlignLeft);
	}
	// добавляем поля на слой
	layoutSparesInfo->addWidget(new QLabel(tr("Код")),1,1,Qt::AlignLeft);
	layoutSparesInfo->addWidget(new QLabel(tr("Наименование")),1,2,Qt::AlignLeft);
	layoutSparesInfo->addWidget(new QLabel(tr("Еден.измер.")),1,3,Qt::AlignLeft);
	layoutSparesInfo->addWidget(new QLabel(tr("Кол-во")),1,4,Qt::AlignLeft);
	layoutSparesInfo->addWidget(new QLabel(tr("Стоимость(руб.)")),1,5,Qt::AlignLeft);
	//layoutWorkInfo->addRow(new QLabel(tr("Марка, модель:")), work_code_1);
	sparesInfoBox->setLayout(layoutSparesInfo);

     //bigEditor = new QTextEdit;
     //bigEditor->setPlainText(tr("This widget takes up all the remaining space "
     //                           "in the top-level layout."));

     buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                      | QDialogButtonBox::Cancel);

     connect(buttonBox, SIGNAL(accepted()), this, SLOT(processForm()));
     connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

     QVBoxLayout *mainLayout = new QVBoxLayout;

	mainLayout->addWidget(viseInfoBox);
	mainLayout->addWidget(commonInfoBox);
	mainLayout->addWidget(carInfoBox);
	mainLayout->addWidget(workInfoBox);
	mainLayout->addWidget(sparesInfoBox);

     //mainLayout->addWidget(bigEditor);
     mainLayout->addWidget(buttonBox);
     setLayout(mainLayout);

     setWindowTitle(tr("Заказ-наряд"));
 }

void Dialog::processForm()
{
    /* Check form filds */
/*
    if( (clientFirstName->text()).isEmpty() ) QMessageBox::information(0, "Error", "Please insert FirstName");
    else if( (clientSecondName->text()).isEmpty() ) QMessageBox::information(0, "Error", "Please insert SecondName");
    else if( (clientPhone->text()).isEmpty() ) QMessageBox::information(0, "Error", "Please insert Phone");
    else if( (clientComment->text()).isEmpty() ) QMessageBox::information(0, "Error", "Please insert Comment");
    else
    {
*/
	QString text_x;
	int i, summ_work=0, summ_spares=0,lastInsertId=0;
        QPrinter printer;
	QDate date = QDate::currentDate();

	/********************************/
	/***********add to dbase*********/
	QSettings *settings = new QSettings;
	QString pathToBase = settings->value("base_path").toString();
	QSqlDatabase dbase = QSqlDatabase::addDatabase("QSQLITE");
	dbase.setDatabaseName(pathToBase);
	if (!dbase.open()) {
		QMessageBox::information(0, tr("Ошибка"), tr("Не могу открыть файл базы данных !!! (1)"));
		return;
	} else {
		QSqlQuery a_query;
		QString str_query, str_insert, str_date;
		bool b;

		str_date =  QString("%1.%2.%3").arg(date.day()).arg(date.month()).arg(date.year());
		/****************************/
		/* add in work_report table*/
		str_insert = "INSERT INTO work_report("
					"customer,"
					"employee,"
					"defect,"
					"car_model,"
					"car_age,"
					"car_milage,"
					"car_number,"
					"car_engine,"
					"car_chassis,"
					"car_vin,"
					"date)"
					"VALUES ('%1','%2','%3','%4','%5','%6','%7','%8','%9','%10','%11');";
		str_query = str_insert.arg(customer->text()).\
					arg(employee->text()).\
					arg(defect->text()).\
					arg(car_model->text()).\
					arg(car_age->text()).\
					arg(car_milage->text()).\
					arg(car_number->text()).\
					arg(car_engine->text()).\
					arg(car_chassis->text()).\
					arg(car_vin->text()).\
					arg(str_date);
		b = a_query.exec(str_query);
		if (!b) {
			QMessageBox::information(0, tr("Ошибка"), tr("Не могу добавить запись в базу данных !!! (2)"));
			return;
		}
		str_query = "select last_insert_rowid()";
		b = a_query.exec(str_query);
		if (!b) {
			QMessageBox::information(0, tr("Ошибка"), tr("Не могу добавить запись в базу данных !!! (3)"));
			return;
		}
		a_query.next();
    		lastInsertId = a_query.value(0).toInt();
		/****************************/
		/* add in work_list table*/
		for( int i=0; i < WORK_NUM; i++ ) {
			if( !(  (work_code[i]->text()).isEmpty()&&\
				(work_about[i]->text()).isEmpty()&&\
				(work_num[i]->text()).isEmpty()&&\
				(work_time[i]->text()).isEmpty()&&\
				(work_cost[i]->text()).isEmpty() ) ) {
				str_insert = "INSERT INTO work_list("
						"work_code,"
						"work_about,"
						"work_num,"
						"work_time,"
						"work_cost,"
						"work_report_id)"
						"VALUES ('%1','%2','%3','%4','%5','%6');";
				str_query = str_insert.arg(work_code[i]->text()).\
						arg(work_about[i]->text()).\
						arg(work_num[i]->text()).\
						arg(work_time[i]->text()).\
						arg(work_cost[i]->text()).\
						arg(lastInsertId);
				b = a_query.exec(str_query);
				if (!b) {
					QMessageBox::information(0, tr("Ошибка"), tr("Не могу добавить запись в базу данных !!!(4)"));
					return;
				}
			} //if
		} //for
		/****************************/
		/* add in spares_list table*/
		for( int i=0; i < SPARES_NUM; i++ ) {
			if( !(  (spares_code[i]->text()).isEmpty()&&\
				(spares_about[i]->text()).isEmpty()&&\
				(spares_num[i]->text()).isEmpty()&&\
				(spares_units[i]->text()).isEmpty()&&\
				(spares_cost[i]->text()).isEmpty() ) ) {
				str_insert = "INSERT INTO spares_list("
						"spares_code,"
						"spares_about,"
						"spares_num,"
						"spares_units,"
						"spares_cost,"
						"work_report_id)"
						"VALUES ('%1','%2','%3','%4','%5','%6');";
				str_query = str_insert.arg(spares_code[i]->text()).\
						arg(spares_about[i]->text()).\
						arg(spares_num[i]->text()).\
						arg(spares_units[i]->text()).\
						arg(spares_cost[i]->text()).\
						arg(lastInsertId);
				b = a_query.exec(str_query);
				if (!b) {
					QMessageBox::information(0, tr("Ошибка"), tr("Не могу добавить запись в базу данных !!!(5)"));
					return;
				}
			} //if
		} //for
	} //else base is opened OK
	/********************************/
	/*LAST_INSERT_ID()*/
	/********************************/

        printer.setPaperSize(QPrinter::A4);
        QPrintDialog printDialog(&printer);
        if (printDialog.exec() == QDialog::Accepted)
        {
		QPainter painter;
		if (! painter.begin(&printer))
		{
			qWarning("failed to open file, is it writable?");
		}
//**************

QTextDocument *doc = new QTextDocument(this);
doc->setUndoRedoEnabled(false);
doc->setPageSize(printer.pageRect().size());
doc->setDefaultFont(QFont("Arial",10,QFont::Normal));
text_x.append("\
<html>\
	<basefont face='Arial' size='12'>\
	<div align=left\
		<font size=+2> AntiHiJack </font>\
	</div>\
	<div align=center>\
        <b> Заказ-наряд № ");
text_x.append(QString("%1").arg(lastInsertId));
text_x.append("\
        </b>\
	</div>\
	<br>\
	<table border=1 bordercolor=0 cellspacing=0 width=100%>\
		<tr>\
			<td><b>Заказчик</b>\
				: \
				<i>"+customer->text()+"</i>\
			    <br><b>Исполнитель</b>\
				: \
				<i>"+employee->text()+"</i>\
			<td><b>Описание дефекта</b>\
				<br>\
				<i>"+defect->text()+"</i>\
		<tr>\
			<td><b>Марка, модель</b> <i>"+car_model->text()+"</i>\
			<td><b>Двигатель №</b> <i>"+car_engine->text()+"</i>\
		<tr>\
			<td><b>Год выпуска</b> <i>"+car_age->text()+"</i> <b>Пробег</b> <i>"+car_milage->text()+"</i>\
			<td><b>Шасси №</b> <i>"+car_chassis->text()+"</i>\
		<tr>\
			<td><b>Государственный рег.номер</b> <i>"+car_number->text()+"</i>\
			<td><b>VIN</b> <i>"+car_vin->text()+"</i>\
	</table>\
	<br>\
	<div align=left>\
		<font> <b>Работы:</b> </font>\
	</div>	\
	<table border=1 cellspacing=0 width=100%>\
		<tr>\
			<td><b>Код</b><td><b>Наименование работ<b><td><b>Норма времени н/ч</b><td><b>Кол-во</b><td><b>Стоимость (руб.)</b><td><b>Сумма (руб.)</b>");

summ_work = 0;
for(i=0; i<5; i++) {
	int summ=0;
        text_x.append("\
                <tr>\
                        <td><i>"+work_code[i]->text()+"</i>\
                        <td><i>"+work_about[i]->text()+"</i>\
                        <td><i>"+work_time[i]->text()+"</i>\
                        <td><i>"+work_num[i]->text()+"</i>\
                        <td><i>"+work_cost[i]->text()+"</i>");

	if( (work_cost[i]->text()).toInt()!=0) {
		if((work_num[i]->text()).toInt()==0) {
			summ = (work_cost[i]->text()).toInt();
		} else {
			summ = work_num[i]->text().toInt()*(work_cost[i]->text()).toInt();
		}
		summ_work+=summ;
		text_x.append(" <td><i>"+QString("%1").arg(summ)+"</i>");
	} else {
		text_x.append(" <td><i></i>");
	}
}

text_x.append("\
	</table>\
	<div align=right>\
");
text_x.append(QString("<font> <b>Итого работы (руб.):</b> %1 </font>").arg(summ_work));
text_x.append("\
	</div>\
	<br>\
	<div align=left>\
		<font> <b>Запасные части:</b> </font>\
	</div>	\
	<table border=1 cellspacing=0 width=100%>\
		<tr>\
			<td><b>Код</b><td><b>Наименование</b><td><b>Един. измер.</b><td><b>Кол-во</b><td><b>Стоимость (руб.)</b><td><b>Сумма (руб.)</b>");

summ_spares = 0;
for(i=0; i<5; i++) {
	int summ=0;
	text_x.append("\
                <tr>\
                        <td><i>"+spares_code[i]->text()+"</i>\
                        <td><i>"+spares_about[i]->text()+"</i>\
                        <td><i>"+spares_units[i]->text()+"</i>\
                        <td><i>"+spares_num[i]->text()+"</i>\
                        <td><i>"+spares_cost[i]->text()+"</i>");
        if( (spares_cost[i]->text()).toInt()!=0) {
                if((spares_num[i]->text()).toInt()==0) {
                        summ = (spares_cost[i]->text()).toInt();
                } else {
                        summ = spares_num[i]->text().toInt()*(spares_cost[i]->text()).toInt();
                }
                summ_spares+=summ;
                text_x.append(" <td><i>"+QString("%1").arg(summ)+"</i>");
        } else {
                text_x.append(" <td><i></i>");
        }
}

text_x.append("\
	</table>\
	<div align=right>\
");
text_x.append(QString("<font> <b>Итого запасные части (руб.):</b> %1</font>").arg(summ_spares));
text_x.append("\
	</div>	\
	<br> \
	<div align=right>\
");
text_x.append(QString("<font> <b>Общая сумма(руб.):</b> %1</font>").arg(summ_spares+summ_work));
text_x.append("\
	</div>	\
	<br>\
	<br>\
	<br>\
	<div align=left>\
		<font size=-1> Гарантийный срок - 36 месяцев. </font><br>\
		<font size=-1> Гарантия осуществляется по месту установки. </font>\
		<font size=-1> исполнитель не несет ответственности за оборудование, поврежденное механически, или в результате действия третьих лиц. Исполнитель освобождается от материальной ответственности в случае устранения неисправностей, связанных с работой Исполнителя, в других сервисах, без предварительного согласования с Исполнителем. </font>\
	</div>\
	<br>\
	<br>\
	<br>\
	<div align=right>\
		<b> Претензий не имею. С рекомендациями по </b><br>\
		<b> использованию результатов работы ознакомлен. </b><br>\
		<b> С окончательной суммой оплаты согласен. </b><br>\
	</div>\
	<br>\
	<br>\
	<br>\
	<table width=100%>\
		<tr>\
			<td align=center><b>Исполнитель</b>\
			<td align=center><b>Заказчик</b>\
		<tr>\
			<td>&nbsp;<br>&nbsp;\
			<td>&nbsp;<br>&nbsp;\
		<tr>\
			<td>________________/_______________________________/\
			<td>________________/_______________________________/\
		<tr>\
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				Подпись\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				Ф.И.О.\
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				Подпись\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
				Ф.И.О.\
		<tr>\
			<td>&nbsp;<br>&nbsp;<br>&nbsp;\
			<td>&nbsp;<br>&nbsp;<br>&nbsp;\
		<tr>\
			<td align=center><b>М.П.</b>\
			<td align=center><b><_____>_________________________________20____г.</b>\
		<tr>\
			<td>&nbsp;<br>&nbsp;\
	</table>\
\
</html>\
");

doc->setHtml(text_x);
//doc->setTextWidth(width());
//doc->setUseDesignMetrics(true);
//doc->setDefaultTextOption ( QTextOption (Qt::AlignHCenter ) );
 
painter.setRenderHint(QPainter::Antialiasing, true);
doc->drawContents(&painter);
painter.end();
//**************

	}
	//QMessageBox msgBox;
	//msgBox.setText("The document has been modified.");
	//msgBox.show();
	//QMessageBox::information(0, "Information", myTestEdit->text());
}

