 #ifndef DIALOG_H
 #define DIALOG_H

 #include <QDialog>
#include <QSettings>
#include <QtSql>

#define WORK_NUM 5
#define SPARES_NUM 5

 class QAction;
 class QDialogButtonBox;
 class QGroupBox;
 class QLabel;
 class QLineEdit;
 class QMenu;
 class QMenuBar;
 class QPushButton;
 class QTextEdit;

class Dialog : public QDialog {
	Q_OBJECT

	public:
		Dialog();

	private slots:
		void processForm();

	private:

	QLineEdit *customer;
	QLineEdit *employee;

	QLineEdit *defect;

    QLineEdit *car_model;
    QLineEdit *car_age;
    QLineEdit *car_milage;
    QLineEdit *car_number;

    QLineEdit *car_engine;
    QLineEdit *car_chassis;
    QLineEdit *car_vin;

    QLineEdit *work_code[WORK_NUM];
    QLineEdit *work_about[WORK_NUM];
    QLineEdit *work_num[WORK_NUM];
    QLineEdit *work_time[WORK_NUM];
    QLineEdit *work_cost[WORK_NUM];

    QLineEdit *spares_code[SPARES_NUM];
    QLineEdit *spares_about[SPARES_NUM];
    QLineEdit *spares_units[SPARES_NUM];
    QLineEdit *spares_num[SPARES_NUM];
    QLineEdit *spares_cost[SPARES_NUM];

//    QLineEdit *client_vise;
//    QLineEdit *performer_vise;

     //void createMenu();
     //void createHorizontalGroupBox();
     //void createGridGroupBox();
     //void createFormGroupBox();

     enum { NumGridRows = 3, NumButtons = 4 };

     //QMenuBar *menuBar;
     //QGroupBox *horizontalGroupBox;
    // QGroupBox *gridGroupBox;

    QGroupBox *commonInfoBox;
    QGroupBox *carInfoBox;
    QGroupBox *workInfoBox;
    QGroupBox *sparesInfoBox;
    QGroupBox *viseInfoBox;

     QTextEdit *smallEditor;
     QTextEdit *bigEditor;
     QLabel *labels[NumGridRows];
     QLineEdit *lineEdits[NumGridRows];
     QPushButton *buttons[NumButtons];
     QDialogButtonBox *buttonBox;

     QMenu *fileMenu;
     QAction *exitAction;
 };

 #endif
