#include <QtGui>
#include <QApplication>

#include "mainwindow.h"
#include <QPrintDialog>
#include <QSettings>
#include <QErrorMessage>

#include <QTextCodec>

int main(int argc, char *argv[])
{

	Q_INIT_RESOURCE(sdi);
	QApplication app(argc, argv);

	/* set codec cherecters */
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));
	QTextCodec::setCodecForTr(QTextCodec::codecForName("utf-8"));

	/* describe application */
	app.setApplicationName("AUTO SERVICE");
	app.setOrganizationName("ANTI_HI_JACK");

	//QFont newFont("Courier", 8, QFont::Bold, true);
	//app.setFont(newFont);

	/* start app */
	MainWindow *mainWin = new MainWindow;
	mainWin->setWindowTitle(">>> Anti Hi Jack <<<");
	mainWin->setWindowIcon(QIcon(":/images/icon.png"));
	mainWin->show();
	return app.exec();
}

