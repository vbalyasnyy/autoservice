#ifndef SQLREPORT_H
#define SQLREPORT_H

#include <QDialog>
#include <QSettings>
#include <QtSql>
#include <QtGui>
#include <QErrorMessage>
#include <QFont>
#include <QtGui/QApplication>


 class QAction;
 class QDialogButtonBox;
 class QGroupBox;
 class QLabel;
 class QLineEdit;
 class QMenu;
 class QMenuBar;
 class QPushButton;
 class QTextEdit;

class SqlReport : public QDialog
{
	Q_OBJECT

	public:
		SqlReport();
		void select_by_id(QString id);
	private slots:
		void onTableCellClicked(const QModelIndex & index);

	private:
	
		QGridLayout *sqlReportLayout;

		QLineEdit *common;

		QSqlQueryModel * model;
		QSqlQueryModel * model1;
		QSqlQueryModel * model2;

		QTableView * view;
		QTableView * view1;
		QTableView * view2;

};

#endif
