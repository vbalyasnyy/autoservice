#include <QtGui>
#include <QErrorMessage>
#include <QFont>
#include <QtGui/QApplication>

#include "sql_report.h"

void SqlReport::select_by_id(QString id) {

	QString str_query;

	str_query = QString("SELECT * FROM work_list WHERE work_report_id==%1;").arg(id);
	model1->setQuery(str_query);
	model1->setHeaderData(0, Qt::Horizontal, QObject::trUtf8("№"));
	model1->setHeaderData(1, Qt::Horizontal, QObject::trUtf8("Код"));
	model1->setHeaderData(2, Qt::Horizontal, QObject::trUtf8("Описание"));
	model1->setHeaderData(3, Qt::Horizontal, QObject::trUtf8("Время"));
	model1->setHeaderData(4, Qt::Horizontal, QObject::trUtf8("Количество"));
	model1->setHeaderData(5, Qt::Horizontal, QObject::trUtf8("Стоимость"));
	model1->setHeaderData(6, Qt::Horizontal, QObject::trUtf8("Заказ"));

	view1->setModel(model1);
	//view1->resizeRowsToContents();
	view1->resizeColumnsToContents();

	str_query = QString("SELECT * FROM spares_list WHERE work_report_id==%1;").arg(id);
	model2->setQuery(str_query);
	model2->setHeaderData(0, Qt::Horizontal, QObject::trUtf8("№"));
	model2->setHeaderData(1, Qt::Horizontal, QObject::trUtf8("Код"));
	model2->setHeaderData(2, Qt::Horizontal, QObject::trUtf8("Зап.часть"));
	model2->setHeaderData(3, Qt::Horizontal, QObject::trUtf8("Единицы"));
	model2->setHeaderData(4, Qt::Horizontal, QObject::trUtf8("Количество"));
	model2->setHeaderData(5, Qt::Horizontal, QObject::trUtf8("Стоимость"));
	model2->setHeaderData(6, Qt::Horizontal, QObject::trUtf8("Заказ"));

	view2->setModel(model2);
	//view2->resizeRowsToContents();
	view2->resizeColumnsToContents();

}

void SqlReport::onTableCellClicked(const QModelIndex & index) {
	select_by_id(view->model()->data(this->view->model()->index(index.row(),0)).toString());
}

SqlReport::SqlReport()
{
	model = new QSqlQueryModel(0);
	model1 = new QSqlQueryModel(0);
	model2 = new QSqlQueryModel(0);

	view = new QTableView(0);
	view1 = new QTableView(0);
	view2 = new QTableView(0);

	sqlReportLayout = new QGridLayout;
	QString numWorkReport = 0;

	QSettings *settings = new QSettings;
	QString pathToBase = settings->value("base_path").toString();
	QSqlDatabase dbase = QSqlDatabase::addDatabase("QSQLITE");
	dbase.setDatabaseName(pathToBase);
	if (!dbase.open()) {
		QMessageBox::information(0, tr("Ошибка"), tr("Не могу открыть файл базы данных !!! (1)"));
		return;
	} else {
		QSqlQuery a_query;
		QString str_query, str_insert, str_date;

		model->setQuery("SELECT * FROM work_report;");
		model->setHeaderData(0, Qt::Horizontal, QObject::trUtf8("№"));
		model->setHeaderData(1, Qt::Horizontal, QObject::trUtf8("Исполнитель"));
		model->setHeaderData(2, Qt::Horizontal, QObject::trUtf8("Клиент"));
		model->setHeaderData(3, Qt::Horizontal, QObject::trUtf8("Дефект"));
		model->setHeaderData(4, Qt::Horizontal, QObject::trUtf8("Модель"));
		model->setHeaderData(5, Qt::Horizontal, QObject::trUtf8("Год"));
		model->setHeaderData(6, Qt::Horizontal, QObject::trUtf8("Пробег"));
		model->setHeaderData(7, Qt::Horizontal, QObject::trUtf8("Рег.Номер"));
		model->setHeaderData(8, Qt::Horizontal, QObject::trUtf8("№ двигателя"));
		model->setHeaderData(9, Qt::Horizontal, QObject::trUtf8("№ шфсси"));
		model->setHeaderData(10, Qt::Horizontal, QObject::trUtf8("VIN"));
		model->setHeaderData(11, Qt::Horizontal, QObject::trUtf8("Дата"));

		view->setModel(model);
		view->resizeRowsToContents();
		view->resizeColumnsToContents();

		select_by_id("1");
	} //else base is opened OK

	view->selectRow(0);
	view->setSelectionMode(QAbstractItemView::SingleSelection);
	view->setSelectionBehavior(QAbstractItemView::SelectRows);
	connect(view, SIGNAL(clicked ( const QModelIndex & )), this, SLOT(onTableCellClicked(const QModelIndex &)));

//	sqlReportLayout->addWidget(view, Qt::AlignHCenter);
	sqlReportLayout->addWidget(view, 0, 0, 1, 2);
	sqlReportLayout->addWidget(view1, 1, 0);
	sqlReportLayout->addWidget(view2, 1, 1);
	//sqlReportLayout->addWidget(modesGroup, 2, 0);

	setLayout(sqlReportLayout);
}

