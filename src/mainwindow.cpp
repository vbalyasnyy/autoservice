#include <QtGui>

#include "mainwindow.h"
#include "workReport.h"
#include "sql_report.h"
#include <QPrintDialog>
#include "version.h"

MainWindow::MainWindow()
{
    /* check that dbase file set's */
    settings = new QSettings;
    /* read last path to dbase */
    QString PathToBase = settings->value("base_path").toString();
    //QMessageBox::information(0, "Information", PathToBase);
    /* ask to set path to file */
    while ( ! QFile::exists(PathToBase) )
    {
        //QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("DBase (*.sqlite)"));
        //QMessageBox::information(0, "Information", fileName);
        QFileDialog *baseFileDialog = new QFileDialog;
        connect(baseFileDialog, SIGNAL(rejected()), this, SLOT(badBaseFile()));
        QString fileName = baseFileDialog->getOpenFileName(this, tr("Open File"),"",tr("DBase (*.sqlite)"));
        //connect(baseFileDialog, SIGNAL(rejected()), this, SLOT(badBaseFile()));
        if( fileName.isEmpty() ) exit(1);
        settings->setValue("base_path", fileName);
        PathToBase = settings->value("base_path").toString();
    }
    /* Echoed dbase file name */
    //QMessageBox::information(0, "Information", PathToBase);
    init();
}

void MainWindow::badBaseFile()
{
    QMessageBox::information(0, "Information", "This is the end");
    exit(1);
}

/*
 void MainWindow::closeEvent(QCloseEvent *event)
 {
     if (maybeSave()) {
         writeSettings();
         event->accept();
     } else {
         event->ignore();
     }
 }
*/

void MainWindow::createWorkReportForm()
{
	//scrollArea = new QScrollArea;
	myWidget = new Dialog;

	//scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidget(myWidget);
	setCentralWidget(scrollArea);
}

void MainWindow::createSqliteReportCommon() {
	commonReport = new SqlReport();
	showReport();
}

void MainWindow::showReport() {
	//scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidget(commonReport);
	setCentralWidget(scrollArea);
	
	mainLayout->addWidget(commonReport);
	scrollArea->setLayout(mainLayout);
}

void MainWindow::fileDbSet()
{
    /* create settings obj */
    settings = new QSettings;
    QFileDialog *baseFileDialog = new QFileDialog;
    QString fileName = baseFileDialog->getOpenFileName(this, tr("Файл БД"),"",tr("DBase (*.sqlite)"));
    if( !fileName.isEmpty() )
        settings->setValue("base_path", fileName);
}

void MainWindow::aboutSet()
{
    QMessageBox::about(this, tr("Компания AntiHiJack"),
             tr("<b>Адрес:</b> Москва, М. Калужская, ул. Профсоюзная 63.<br><b>Версия:</b> %1").arg(VERSION));
}

 void MainWindow::printerSet()
 {

/*   QPrinter printer;

    printer.setPaperSize(QPrinter::A4);

    QPrintDialog printDialog(&printer);
    if (printDialog.exec() == QDialog::Accepted) { 


     QPainter painter;
     if (! painter.begin(&printer)) { // failed to open file
         qWarning("failed to open file, is it writable?");
     }
     painter.drawText(10, 10, "Test");
     if (! printer.newPage()) {
         qWarning("failed in flushing page to disk, disk full?");
     }
     painter.drawText(10, 10, "Test 2");
     painter.end();

    }
*/
 }


/* bool MainWindow::save()
 {
     if (isUntitled) {
         return saveAs();
     } else {
         return saveFile(curFile);
     }
 }
*/
 bool MainWindow::saveAs()
 {
     QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                                                     curFile);
     if (fileName.isEmpty())
         return false;

     return saveFile(fileName);
 }

 void MainWindow::documentWasModified()
 {
     setWindowModified(true);
 }

void MainWindow::init()
{
    setAttribute(Qt::WA_DeleteOnClose);

    //isUntitled = true;

    textEdit = new QTextEdit;
    myWidget = new Dialog;


    //setCentralWidget(myWidget);

    createActions();
    createMenus();
    //createToolBars();
    createStatusBar();

    readSettings();

    connect(textEdit->document(), SIGNAL(contentsChanged()),
        this, SLOT(documentWasModified()));

    setUnifiedTitleAndToolBarOnMac(true);

	scrollArea = new QScrollArea;

	mainLayout = new QVBoxLayout();

}

void MainWindow::createActions()
{
    /*  */
    printerSettings = new QAction(QIcon(":/images/new.png"), tr("&Printer"), this);
     //printerSettings->setShortcuts(QKeySequence::Printer);
     printerSettings->setStatusTip(tr("Printer settings"));
     connect(printerSettings, SIGNAL(triggered()), this, SLOT(printerSet()));

    /* DB file changed */
    //fileDbSettings = new QAction(QIcon(":/images/new.png"),tr("&FileDB"), this);
    fileDbSettings = new QAction(tr("&Файл БД"), this);
    fileDbSettings->setStatusTip(tr("Сменить файл БД"));
    connect(fileDbSettings, SIGNAL(triggered()), this, SLOT(fileDbSet()));    

//    QAction *aboutSettings;

     workReport = new QAction(QIcon(":/images/new.png"), tr("&Заказ-наряд"), this);
     //printerSettings->setShortcuts(QKeySequence::Printer);
     workReport->setStatusTip(tr("Создать заказ наряд"));
     connect(workReport, SIGNAL(triggered()), this, SLOT(createWorkReportForm()));

     sqliteReport_common = new QAction(QIcon(":/images/new.png"), tr("&Общий"), this);
     //printerSettings->setShortcuts(QKeySequence::Printer);
     sqliteReport_common->setStatusTip(tr("Смотреть базу заказ-нарядов"));
     connect(sqliteReport_common, SIGNAL(triggered()), this, SLOT(createSqliteReportCommon()));
/*
     sqliteReport_work = new QAction(QIcon(":/images/new.png"), tr("&Работы"), this);
     //printerSettings->setShortcuts(QKeySequence::Printer);
     sqliteReport_work->setStatusTip(tr("Смотреть базу выполненных работ"));
     connect(sqliteReport_work, SIGNAL(triggered()), this, SLOT(createSqliteReportWork()));

     sqliteReport_spares = new QAction(QIcon(":/images/new.png"), tr("&Оборудование"), this);
     //printerSettings->setShortcuts(QKeySequence::Printer);
     sqliteReport_spares->setStatusTip(tr("Смотреть базу оборудования"));
     connect(sqliteReport_spares, SIGNAL(triggered()), this, SLOT(createSqliteReportSpares()));
*/
     /*QAction *fileDB;
     QAction *workReport;
     QAction *equipmentSale;
     QAction *equipmentAdd;
     QAction *reportDEfault;
     QAction *loginAct;
     QAction *moneyAct;*/

/*
     newAct = new QAction(QIcon(":/images/new.png"), tr("&New"), this);
     newAct->setShortcuts(QKeySequence::New);
     newAct->setStatusTip(tr("Create a new file"));
     connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

     saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
     saveAct->setShortcuts(QKeySequence::Save);
     saveAct->setStatusTip(tr("Save the document to disk"));
     connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

     saveAsAct = new QAction(tr("Save &As..."), this);
     saveAsAct->setShortcuts(QKeySequence::SaveAs);
     saveAsAct->setStatusTip(tr("Save the document under a new name"));
     connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

     closeAct = new QAction(tr("&Close"), this);
     closeAct->setShortcut(tr("Ctrl+W"));
     closeAct->setStatusTip(tr("Close this window"));
     connect(closeAct, SIGNAL(triggered()), this, SLOT(close()));
*/
     exitAct = new QAction(tr("В&ыход"), this);
     exitAct->setShortcuts(QKeySequence::Quit);
     exitAct->setStatusTip(tr("Закрыть программу"));
     connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
/*
     cutAct = new QAction(QIcon(":/images/cut.png"), tr("Cu&t"), this);
     cutAct->setShortcuts(QKeySequence::Cut);
     cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                             "clipboard"));
     connect(cutAct, SIGNAL(triggered()), textEdit, SLOT(cut()));

     copyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
     copyAct->setShortcuts(QKeySequence::Copy);
     copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                              "clipboard"));
     connect(copyAct, SIGNAL(triggered()), textEdit, SLOT(copy()));

     pasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
     pasteAct->setShortcuts(QKeySequence::Paste);
     pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                               "selection"));
     connect(pasteAct, SIGNAL(triggered()), textEdit, SLOT(paste()));

     aboutAct = new QAction(tr("&About"), this);
     aboutAct->setStatusTip(tr("Show the application's About box"));
     connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
*/
     aboutSettings = new QAction(tr("&О программе"), this);
     aboutSettings->setStatusTip(tr("О программе"));
     connect(aboutSettings, SIGNAL(triggered()), this, SLOT(aboutSet()));
/*
     cutAct->setEnabled(false);
     copyAct->setEnabled(false);
     connect(textEdit, SIGNAL(copyAvailable(bool)),
             cutAct, SLOT(setEnabled(bool)));
     connect(textEdit, SIGNAL(copyAvailable(bool)),
             copyAct, SLOT(setEnabled(bool)));
    */ 
}

void MainWindow::createMenus()
{
    settingsMenu = menuBar()->addMenu(tr("&Настройки"));
    //settingsMenu->addAction(printerSettings);
    settingsMenu->addAction(fileDbSettings);
    settingsMenu->addAction(aboutSettings);

    workMenu = menuBar()->addMenu(tr("&Работы"));
    workMenu->addAction(workReport);

    //equipmentMenu = menuBar()->addMenu(tr("&Оборудование"));
	reportMenu = menuBar()->addMenu(tr("&Отчеты"));
	reportMenu->addAction(sqliteReport_common);
//	reportMenu->addAction(sqliteReport_work);
//	reportMenu->addAction(sqliteReport_spares);

    //loginMenu = menuBar()->addMenu(tr("&Авторизация"));
    //expenseMenu = menuBar()->addMenu(tr("&Expense"));
     
    exitMenu = menuBar()->addMenu(tr("&Выход"));
    exitMenu->addAction(exitAct);

/*     fileMenu = menuBar()->addMenu(tr("&File"));
     fileMenu->addAction(newAct);
     fileMenu->addAction(openAct);
     fileMenu->addAction(saveAct);
     fileMenu->addAction(saveAsAct);
     fileMenu->addSeparator();
     fileMenu->addAction(closeAct);
     fileMenu->addAction(exitAct);

     editMenu = menuBar()->addMenu(tr("&Edit"));
     editMenu->addAction(cutAct);
     editMenu->addAction(copyAct);
     editMenu->addAction(pasteAct);

     menuBar()->addSeparator();

     helpMenu = menuBar()->addMenu(tr("&Help"));
     helpMenu->addAction(aboutAct);
     helpMenu->addAction(aboutQtAct);
*/
 }

 void MainWindow::createToolBars()
 {
     fileToolBar = addToolBar(tr("File"));
     fileToolBar->addAction(newAct);
     fileToolBar->addAction(openAct);
     fileToolBar->addAction(saveAct);

     editToolBar = addToolBar(tr("Edit"));
     editToolBar->addAction(cutAct);
     editToolBar->addAction(copyAct);
     editToolBar->addAction(pasteAct);
 }

/* create start text line */
void MainWindow::createStatusBar()
{
    /* check that dbase file set's */
    settings = new QSettings;
    /* read last path to dbase */
    QString PathToBase = settings->value("base_path").toString();
    QString intro = "Файл БД: ";
    intro.append(PathToBase);
    /* show string in bar */
    statusBar()->showMessage(intro);
}

 void MainWindow::readSettings()
 {
     QSettings settings;
     QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
     QSize size = settings.value("size", QSize(400, 400)).toSize();
     move(pos);
     resize(size);
 }

 void MainWindow::writeSettings()
 {
     QSettings settings;
     settings.setValue("pos", pos());
     settings.setValue("size", size());
 }

/*
 bool MainWindow::maybeSave()
 {
     if (textEdit->document()->isModified()) {
         QMessageBox::StandardButton ret;
         ret = QMessageBox::warning(this, tr("SDI"),
                      tr("The document has been modified.\n"
                         "Do you want to save your changes?"),
                      QMessageBox::Save | QMessageBox::Discard
                      | QMessageBox::Cancel);
         if (ret == QMessageBox::Save)
             return save();
         else if (ret == QMessageBox::Cancel)
             return false;
     }
     return true;
 }
*/

 bool MainWindow::saveFile(const QString &fileName)
 {
     QFile file(fileName);
     if (!file.open(QFile::WriteOnly | QFile::Text)) {
         QMessageBox::warning(this, tr("SDI"),
                              tr("Cannot write file %1:\n%2.")
                              .arg(fileName)
                              .arg(file.errorString()));
         return false;
     }

     QTextStream out(&file);
     QApplication::setOverrideCursor(Qt::WaitCursor);
     out << textEdit->toPlainText();
     QApplication::restoreOverrideCursor();

     statusBar()->showMessage(tr("File saved"), 2000);
     return true;
 }


 QString MainWindow::strippedName(const QString &fullFileName)
 {
     return QFileInfo(fullFileName).fileName();
 }

 MainWindow *MainWindow::findMainWindow(const QString &fileName)
 {
     QString canonicalFilePath = QFileInfo(fileName).canonicalFilePath();

     foreach (QWidget *widget, qApp->topLevelWidgets()) {
         MainWindow *mainWin = qobject_cast<MainWindow *>(widget);
         if (mainWin && mainWin->curFile == canonicalFilePath)
             return mainWin;
     }
     return 0;
 }
