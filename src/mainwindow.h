 #ifndef MAINWINDOW_H
 #define MAINWINDOW_H

#include <QSettings>
 #include <QMainWindow>
#include "workReport.h"
#include "sql_report.h"
#include <QScrollArea>

 class QAction;
 class QMenu;
 class QTextEdit;

class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		MainWindow();

	protected:
		//void closeEvent(QCloseEvent *event);

	private slots:
		void badBaseFile();

		/* Settings */
		void printerSet();
		void fileDbSet();
		void aboutSet();
		/* Work */
		void createWorkReportForm();
		/* Report */
		void createSqliteReportCommon();
//		void createSqliteReportWork();
//		void createSqliteReportSpares();
		void showReport();

		//void newFile();
		//bool save();
		bool saveAs();
		void documentWasModified();

	private:
		void init();
		void createActions();
		void createMenus();
		void createToolBars();
		void createStatusBar();
		void readSettings();
		void writeSettings();
		//bool maybeSave();
		bool saveFile(const QString &fileName);
		QString strippedName(const QString &fullFileName);
		MainWindow *findMainWindow(const QString &fileName);

		QVBoxLayout *mainLayout;

		QTextEdit *textEdit;
		Dialog *myWidget;
		SqlReport *commonReport;
		QScrollArea *scrollArea;
		QString curFile;
		//bool isUntitled;

		/* Пункты меню */
		QMenu *settingsMenu;
		QMenu *workMenu;
		QMenu *equipmentMenu;
		QMenu *expenseMenu;
		QMenu *reportMenu;
		QMenu *loginMenu;
		QMenu *exitMenu;

		//QMenu *fileMenu;
		//QMenu *editMenu;
		//QMenu *helpMenu;
		QToolBar *fileToolBar;
		QToolBar *editToolBar;

		/* События в меню */
		QAction *printerSettings;
		QAction *fileDbSettings;
		QAction *aboutSettings;

		QAction *workReport;
		QAction *sqliteReport_common;
		QAction *sqliteReport_work;
		QAction *sqliteReport_spares;

		QAction *equipmentSale;
		QAction *equipmentAdd;
		QAction *equipmentConfig;

		QAction *expenseAdd;
		QAction *expenseConfig;

		QAction *reportDefault;

		QAction *loginAct;

		QAction *newAct;
		QAction *openAct;
		QAction *saveAct;
		QAction *saveAsAct;
		QAction *closeAct;
		QAction *exitAct;
		QAction *cutAct;
		QAction *copyAct;
		QAction *pasteAct;
		QAction *aboutAct;
		QAction *aboutQtAct;

		/* Настройки */    
		QSettings *settings;
 };

 #endif
