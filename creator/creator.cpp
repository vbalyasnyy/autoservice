#include <QtGui/QApplication>
#include <QtSql>

int main(/*int argc, char *argv[]*/)
{
    //QCoreApplication app(argc, argv);

    QSqlDatabase dbase = QSqlDatabase::addDatabase("QSQLITE");
    dbase.setDatabaseName("empty.sqlite");
    if (!dbase.open()) {
        qDebug() << "Can't opeb database file.";
        return -1;
    }

    QSqlQuery a_query;
    // DDL query
	QString str = "CREATE TABLE work_report ("
			"id integer PRIMARY KEY NOT NULL, "
			"employee VARCHAR(50), "
			"customer VARCHAR(50), "
			"defect VARCHAR(300), "
			"car_model VARCHAR(50), "
			"car_age VARCHAR(4), "
			"car_milage bigint, "
			"car_number VARCHAR(10), "
			"car_engine VARCHAR(30), "
			"car_chassis VARCHAR(30), "
			"car_vin VARCHAR(30), "
			"date VARCHAR(10)"
			");";
    bool b = a_query.exec(str);
    if (!b) {
        qDebug() << "Can't create table.";
    }

        str = "CREATE TABLE work_list ("
                        "id integer PRIMARY KEY NOT NULL, "
                        "work_code VARCHAR(10), "
                        "work_about VARCHAR(100), "
                        "work_time VARCHAR(10), "
                        "work_num INT, "
                        "work_cost INT, "
                        "work_report_id INT"
                        ");";
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "Can't create table.";
    }

        str = "CREATE TABLE spares_list ("
                        "id integer PRIMARY KEY NOT NULL, "
                        "spares_code VARCHAR(10), "
                        "spares_about VARCHAR(10), "
                        "spares_units VARCHAR(10), "
                        "spares_num INT, "
                        "spares_cost INT, "
                        "work_report_id INT"
                        ");";
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "Can't create table.";
    }



/*
    // DML
    QString str_insert = "INSERT INTO my_table(number, address, age) "
            "VALUES (%1, '%2', %3);";
    str = str_insert.arg("14")
            .arg("hello world str.")
            .arg("37");
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "Кажется данные не вставляются, проверьте дверь, может она закрыта?";
    }
    //.....
    if (!a_query.exec("SELECT * FROM my_table")) {
        qDebug() << "Даже селект не получается, я пас.";
        return -2;
    }
    QSqlRecord rec = a_query.record();
    int number = 0,
            age = 0;
    QString address = "";

    while (a_query.next()) {
        number = a_query.value(rec.indexOf("number")).toInt();
        age = a_query.value(rec.indexOf("age")).toInt();
        address = a_query.value(rec.indexOf("address")).toString();

        qDebug() << "number is " << number
                 << ". age is " << age
                 << ". address" << address;
    }
*/
    return 0;//app.exec();
}

